class ServerRout {
  static String baseUrl = "http://13.214.207.172:8001/";

  static String login = "api/auth/login";

  static String basicToken = "Basic ZzM6MTIz";

  static String keyToken = 'token';

  static String profilePath = "api/user/profile";

  static String updatePath = "api/user/profile-avatar/token";

  static  String userID = "1";

  static String uploadPicsFirst  = "api/user/";

  static String uploadPicsEnd  = "/profileAvatar";

  static String uploadPicPath = "api/user/profile-avatar/token";

  static String changePassPath = "api/user/password/reset";

  static String popularFoodPath = "api/foods/list";

  static String categoryPath = "api/categories";

  static String allFoodPath = "api/foods";

  static String cartKey = "cart_key";

  static String tablePath = "api/tables";

  static String bookingPath = "api/orders";

  static String fav_Key = "fav-key";

  static String locationPath = "api/address";

  static String locationPathC = "api/address/current";

  static String signUpPath  = "api/user";

  static String orderPath = "api/orders";

  static String registerToken = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVJZCI6MSwiaXNzIjoiS0lMTy1JVCIsImF2YXRhciI6Imh0dHBzOi8vczMuYXAtc291dGhlYXN0LTIuYW1hem9uYXdzLmNvbS9yZXMuZGF0YS91c2VyLWF2YXRhci8xMTcxNDcyNTg0MjY1MC1zY2FsZWRfMTAwMDI5MzA5NC5qcGciLCJwaG9uZSI6IjA5OTk5OTk5Iiwic2NvcGUiOiJsaXN0LXVzZXIgY3JlYXRlLXJvbGUgZGVsZXRlLXJvbGUgY3JlYXRlLXVzZXIgZWRpdC11c2VyIGVkaXQtcm9sZSBsaXN0LXJvbGUgZGVsZXRlLXVzZXIiLCJuYW1lIjoiTW9ua2V5IEQgTHVmZnkiLCJyb2xlTmFtZSI6IlN1cGVyLUFkbWluIiwiaWQiOjEsImV4cCI6MTcxNTY2NDY3MCwiaWF0IjoxNzE1MDU5ODcwLCJqdGkiOiIxIiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJ1c2VybmFtZSI6ImFkbWluIiwic3RhdHVzIjp0cnVlfQ.ePMKVOtb0DohON6KTk3SuWca0HduMMXt0h0kroZhH2kVeS-5C8h63WHxTE81T454q5avvZHx9fNhWd1FcbUhpYX4nJPhn6SFMnaNul8ZXghj4Mua5WtK7vinwVDqlUI7mwOteLqgG-93yEzQKCEmf1COSRVEAqhzwwbofyDc41Ug2HJSH2aUDE-uH7lN9kFLjhm4md_cwq88UUJEa62RLWLY9NUVFE436Q1UwhBCJi0arHDhJD3b5O5R-JMd_2x2pRMrUUQf7-7wFzMTCxk1YbNazw5UFTBt78ZZBApEn6C-sM4AM8-71sh1JtysRsl2mcYVI3hBUOuLT6LOMh4Oww";
}
