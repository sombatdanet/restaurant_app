import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant_app/components/custom_field_view.dart';
import 'package:restaurant_app/core/utils/size_utils.dart';
import 'package:restaurant_app/screens/home_screen/controller/home_page_controller.dart';
import 'package:restaurant_app/themes/theme_helper.dart';
import '../../constants/image_constant.dart';
import '../../themes/app_decoration.dart';
import '../../widgets/menu_widget.dart';
import 'controller/search_controller.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<SearchScreenController>();
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
            padding:  EdgeInsets.only(left: 12.h),
            child:  IconButton(
                onPressed: (){
                  onTapVector();
                },
                icon:   Icon(Icons.arrow_back_ios,color:appTheme.black900,size: 18.h,)
            )),
        centerTitle: true,
        title: Text(
          "Search",
          style: theme.textTheme.titleLarge!.copyWith(
            color: appTheme.black900,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
      backgroundColor: appTheme.gray50,
      body: SizedBox(
        width: SizeUtils.width,
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.h),
            child: CustomFieldView(
              onChanged: (value) {
                controller.searchUpdate(value);
              },
              hintText: "Search by keyboard or categories".tr,
              prefix: Padding(
                padding: EdgeInsets.only(
                  right: 16.h,
                  left: 16.h,
                ),
                child:  Icon(
                  Icons.search,
                  color: Colors.grey.shade600,
                ),
              ),
            ),
          ),
          Expanded(child: _searchBody())
        ]),
      ),
    );
  }
}

Widget _searchBody() {
  final controller = Get.find<SearchScreenController>();
  final homeController = Get.find<HomeController>();
  return Obx(
    () => homeController.isLoading.value
        ? ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 16.v),
            scrollDirection: Axis.vertical,
            separatorBuilder: (
              context,
              index,
            ) {
              return SizedBox(
                height: 13.h,
              );
            },
            itemCount: controller.listForSearch.length,
            itemBuilder: (context, index) {
              final list = controller.listForSearch[index];
              return MenuCardItemWidget(
                dataPopular: list,
                title: list.name!,
                price: list.price.toString(),
                discountPrice: list.discount.toString(),
                offerPercentage: "20% off",
                imagePath: list.foodImage ?? ImageConstant.imageBurger,
                smallPill: "Chef pick",
              );
            },
          )
        : _buildShimmer() ,
  );
}

onTapVector() {
  Get.back();
}
Widget _buildShimmer() {
  return Column(
    children: [
      SizedBox(
        height: 50.v,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          itemBuilder: (context, index) {
            return Container(
              margin:
              const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 50.v,
              width: 120.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadiusStyle.roundedBorder15,
                color: Colors.grey.withOpacity(0.1),
              ),
            );
          },
        ),
      ),
      Expanded(
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: 5,
          itemBuilder: (context, index) {
            return Row(
              children: [
                Container(
                  margin:  EdgeInsets.symmetric(horizontal: 12.h,vertical: 12.v),
                  width: 110.h,
                  height: 110.v,
                  decoration: AppDecoration.fillWhiteA.copyWith(
                    borderRadius: BorderRadiusStyle.roundedBorder10,
                    boxShadow: [AppDecoration.boxShadow],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 2.v),
                      Container(
                        height: 20.v,
                        width: 40.h,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadiusStyle.roundedBorder15
                        ),
                      ),
                      SizedBox(height: 1.v),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 20.v,
                            width: 40.h,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadiusStyle.roundedBorder15
                            ),
                          ),
                          SizedBox(width: 8.h),
                          Container(
                            margin: EdgeInsets.only(right: 40.h),
                            height: 20.v,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadiusStyle.roundedBorder15
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5.v),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 20.v,
                            width: 80,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadiusStyle.roundedBorder15
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 40.h),
                            height: 20.v,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadiusStyle.roundedBorder15
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5.v),
                      Container(
                        width: 160.h,
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    ],
  );
}
