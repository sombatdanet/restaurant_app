import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant_app/bindings/root_binding.dart';
import 'package:restaurant_app/components/app_bar/appbar_leading_image.dart';
import 'package:restaurant_app/components/app_bar/appbar_subtitle.dart';
import 'package:restaurant_app/components/app_bar/appbar_subtitle_one.dart';
import 'package:restaurant_app/components/app_bar/appbar_title_image.dart';
import 'package:restaurant_app/components/app_bar/custom_app_bar.dart';
import 'package:restaurant_app/components/custom_elevated_button.dart';
import 'package:restaurant_app/components/custom_heading.dart';
import 'package:restaurant_app/components/custom_text_button.dart';
import 'package:restaurant_app/core/utils/size_utils.dart';
import 'package:restaurant_app/screens/home_screen/widgets/delicious_dessert_card.dart';
import 'package:restaurant_app/screens/home_screen/widgets/popular_category_card.dart';
import 'package:restaurant_app/screens/home_screen/widgets/special_product_card.dart';
import 'package:restaurant_app/screens/menu-screen/controller/menu_controller.dart';
import 'package:restaurant_app/screens/user_screen/user_profile_screen/controller/main_profile_controller.dart';
import 'package:restaurant_app/themes/app_decoration.dart';
import 'package:restaurant_app/themes/custom_button_style.dart';
import 'package:restaurant_app/themes/theme_helper.dart';
import '../../../constants/image_constant.dart';
import '../../routes/app_routes.dart';
import '../location_screen/location_screnn.dart';
import 'controller/home_page_controller.dart';

class HomePage extends StatelessWidget {
    HomePage({Key? key}) : super(key: key,);

   @override

   final Connectivity _connectivity = Connectivity();

  @override
  Widget build(BuildContext context) {
    final HomeController controller = Get.find();
    final MenuScreenController menuController = Get.find();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: appTheme.gray5001,
      appBar: _buildAppBar(context),
        body: StreamBuilder(
          stream: _connectivity.onConnectivityChanged,
          builder: (_,snapshot){
            if(snapshot.data?[0] == ConnectivityResult.none){
              return _buildNoInternet();
            }
            controller.initFunction();
            return _buildBody();
          },
        ),
    );
  }

  SizedBox _buildBody() {

    final HomeController controller = Get.find();

    final MainProfileController mainProfileController = Get.find();

    return SizedBox(
      width: SizeUtils.width,
      child: SingleChildScrollView(
        padding: EdgeInsets.only(top: 20.v),
        child: Padding(
          padding: EdgeInsets.only(bottom: 5.v),
          child: Column(
            children: [
              InkWell(
                onTap: (){
                  Get.toNamed(AppRoutes.searchScreen);
                },
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.h,),
                    padding: EdgeInsets.symmetric(vertical: 11.v),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.h),
                      color: appTheme.whiteA700,
                      border: Border.all(
                        color: appTheme.blueGray100,
                        width: 1,
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 15.h,
                          ),
                          child: Icon(
                            Icons.search,
                            color: Colors.grey.shade600,
                            size: 20.adaptSize,
                          ),
                        ),
                        Text("Type a dish or cuisine".tr,style: theme.textTheme.bodyMedium,),
                      ],)
                ),
              ),
              SizedBox(height: 20.v),
              _buildOfferSection(),
              SizedBox(height: 20.v),
              CustomTitleHeading(
                text: 'Popular Categories',
                textButton: CustomTextButton(
                  onPressed: () {
                    controller.routeToSeaAll(controller.listPopularFood,"Popular Categories");
                  },
                ),
              ),
              Obx(()=> controller.isLoading.value?PopularCategoryCard(): _buildShimmerForPopular()),
              CustomTitleHeading(
                text: "Today's Special",
                textButton: CustomTextButton(
                  onPressed: () {
                    controller.routeToSeaAll(controller.listAllFood,"Today's Special");
                  },
                ),
              ),
              Obx(()=> controller.listAllFood.isNotEmpty?SpecialProductItemCard():_buildShimmer()),
              CustomTitleHeading(
                text: 'New ReLease',
                textButton: CustomTextButton(
                  onPressed: () {
                    controller.routeToSeaAll(controller.newRelease,"New ReLease");
                  },
                ),
              ),
              Obx(()=> controller.newRelease.isNotEmpty?DeliciousDessertItemCard():_buildShimmer()),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildOrderButton() {
    return CustomElevatedButton(
      height: 39.v,
      width: 150.h,
      text: "ORDER NOW".tr,
      buttonStyle: CustomButtonStyles.fillWhiteA,
    );
  }

  /// Section Widget
  Widget _buildOfferSection() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 16.h),
      padding: EdgeInsets.symmetric(
        horizontal: 8.h,
        vertical: 35.v,
      ),
      decoration: AppDecoration.gradientOrangeToPink.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder15,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 4.v),
          Text(
            "Up to 40 OFF".tr,
            style: theme.textTheme.headlineMedium,
          ),
          SizedBox(height: 4.v),
          Text(
            "ON YOUR FIRST ORDER".tr.toUpperCase(),
            style: theme.textTheme.bodyLarge,
          ),
          SizedBox(height: 10.v),
          _buildOrderButton(),
        ],
      ),
    );
  }
}
PreferredSizeWidget _buildAppBar(BuildContext context) {
  MainProfileController mainProfileController = Get.find();
  HomeController homeController = Get.find();
  return  CustomAppBar(
    leadingWidth: 200.h,
    leading: InkWell(
      onTap: ()=> displayBottomSheet(context, homeController.locationList),
      child: Row(
        children: [
          AppbarLeadingImage(
            imagePath: ImageConstant.imgLinkedin,
            margin: EdgeInsets.only(
              left: 12.h,
              top: 10.v,
              bottom: 17.v,
            ),
          ),
        Obx(
            ()=> mainProfileController.isLoading.value? Padding(
            padding: EdgeInsets.only(left: 12.h,top: 5.v),
            child: SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      AppbarSubtitle(
                          text: homeController.current.value?.location!.tr ?? "No Location",
                        ),
                      AppbarTitleImage(
                        imagePath: ImageConstant.imgArrowDown,
                        margin: EdgeInsets.only(
                          left: 3.h,
                        ),
                      ),
                    ],
                  ),
                 AppbarSubtitleOne(
                      text: homeController.current.value?.name!.tr?? "No address",
                      margin: EdgeInsets.only(right: 37.h),
                    ),
                ],
              ),
            ),
          ): Container(),
        )
        ],
      ),
    ),
    actions: [
     GestureDetector(
        onTap: ()=>Get.toNamed(AppRoutes.mainProfileScreen),
        child: Obx(
        ()=> mainProfileController.isLoading.value?Container(
            margin: EdgeInsets.fromLTRB(16.h, 12.v, 16.h, 11.v),
            child: mainProfileController.userProfile.value!.data!.avatar == null?const CircleAvatar(
              child: Icon(Icons.person,color: Colors.grey,size: 20,),
            ): CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(mainProfileController.userProfile.value!.data!.avatar!)
            ),
          ):Container(),
        ),
      ),
    ],
  );
}
Widget _buildNoInternet(){
  return  SingleChildScrollView(
    child: Column(
      children: [
        Text("No internet connection",style: theme.textTheme.titleMedium?.copyWith(color: Colors.grey.withOpacity(0.9))),
        Text("Please check your  connection",style: theme.textTheme.titleMedium?.copyWith(color: Colors.grey.withOpacity(0.9),),),
        _buildShimmerForPopular(),
        _buildShimmer(),
        _buildShimmer()
      ],
    ),
  );
}
Widget _buildShimmer(){
  return SizedBox(
    height: 310.v,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
        itemCount: 2,
        itemBuilder: (context,index){
          return Container(
            margin:  EdgeInsets.symmetric(horizontal: 13.h,vertical: 10),
            width: 285.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadiusStyle.roundedBorder15,
              color: Colors.white,
              boxShadow: [AppDecoration.boxShadow],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 145.v,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.1),
                    borderRadius: BorderRadiusStyle.roundedOnlyTopBorder15
                  ),
                ),
                Container(
                  height: 20.v,
                  width: double.maxFinite,
                  margin: EdgeInsets.symmetric(horizontal: 20.h,vertical: 10.v),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadiusStyle.roundedBorder5,
                    color: Colors.grey.withOpacity(0.1),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20.h),
                      height: 20.v,
                      width: 100.h,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadiusStyle.roundedBorder5,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                    ),
                    Container(
                      height: 20.v,
                      width: 50.h,
                      margin: EdgeInsets.only(right: 20.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadiusStyle.roundedBorder5,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                    )
                  ],
                ),
                Container(
                  height: 20.v,
                  width: 100.h,
                  margin: EdgeInsets.symmetric(horizontal: 20.h,vertical: 10.v),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadiusStyle.roundedBorder5,
                    color: Colors.grey.withOpacity(0.1),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 20.v,
                      width: 100.h,
                      margin: EdgeInsets.only(left: 20.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadiusStyle.roundedBorder5,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                    ),
                    Container(
                      height: 20.v,
                      width: 50.h,
                      margin: EdgeInsets.only(right: 20.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadiusStyle.roundedBorder5,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        }
    ),
  );
}
Widget _buildShimmerForPopular(){
  return SizedBox(
    height: 120.v,
    child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (context,index){
          return Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 2.h),
                height: 90.v,
                width: 90.h,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey.withOpacity(0.1),
                ),
              ),
            ],
          );
        }
    ),
  );
}