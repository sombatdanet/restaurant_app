import 'package:get/get.dart';
import 'package:restaurant_app/components/custom_image_view.dart';
import 'package:restaurant_app/core/utils/size_utils.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_app/screens/splash_screen/splash_screen_controller.dart';
import 'package:restaurant_app/themes/custom_text_style.dart';
import 'package:restaurant_app/themes/theme_helper.dart';

import '../../../constants/image_constant.dart';

class SplashScreen extends StatelessWidget {
  SplashScreen({Key? key}) : super(key: key);

  final SplashScreenController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  SizedBox _buildBody() {
    return SizedBox(
        width: double.maxFinite,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(bottom: 11.v),
            child: Column(
              children: [
                CustomImageView(
                  color: appTheme.orange800,
                  imagePath: ImageConstant.imgVector,
                  height: 187.v,
                  width: 143.h,
                  alignment: Alignment.centerLeft,
                ),
                SizedBox(height: 101.v),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomImageView(
                      imagePath: ImageConstant.LOGO,
                      width: 150.adaptSize,
                    )
                  ],
                ),
                SizedBox(height: 36.v),
                SizedBox(
                  child: Text(
                    "Dining and Delivery Restaurant App".tr,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: CustomTextStyles.headlineMediumBlack900.copyWith(
                      height: 1.14,
                    ),
                  ),
                ),
                SizedBox(height: 201.v),
                CustomImageView(
                  color: appTheme.orange800,
                  imagePath: ImageConstant.imgVectorOrange800,
                  height: 191.v,
                  width: 193.h,
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.only(right: 2.h),
                ),
              ],
            ),
          ),
        ),
      );
  }
}
