import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant_app/core/utils/size_utils.dart';
import 'package:restaurant_app/screens/home_screen/controller/home_page_controller.dart';
import 'package:restaurant_app/screens/see_all_screeen/widget/see_all_widget.dart';
import '../../components/app_bar/appbar_title.dart';
import '../../components/app_bar/custom_app_bar.dart';
import '../../constants/image_constant.dart';
import '../../themes/theme_helper.dart';
import '../home_screen/model/product_item_model.dart';

class SeeAllScreen extends StatelessWidget {
  const SeeAllScreen({super.key,});

  @override
  Widget build(BuildContext context) {
    final listSeeAll = Get.arguments['listSeeAll'];
    final HomeController controller = Get.find();
    return Scaffold(
      appBar: _buildAppBar(),
      body: ListView.builder(
        scrollDirection: Axis.vertical,
          itemCount: listSeeAll.length,
          itemBuilder:(context,index){
            ProductItemModel model = controller.homeModelObj.value.specialProductItemList.value[0];
            final list = listSeeAll[index];
            return ProductCartItemWidgetSeeAll(
              onTap: () {
                controller.routeToSpecialProductDetail(model,list.id!.toInt(),list);
              },
              title: list.name!,
              description: list.description??"No description",
              price: list.price?.toString() ?? "0",
              discountPrice: list.discount?.toString() ?? "0",
              offerPercentage: model.offer!.value,
              imagePath:list.foodImage ?? ImageConstant.imgMuttonLambBir,
              dataPopular: list,
            );
          }
      ),
    );
  }
}
PreferredSizeWidget _buildAppBar() {
  final title = Get.arguments['title'];
  return CustomAppBar(
    centerTitle: true,
    leading: Padding(
        padding:  EdgeInsets.only(left: 12.h),
        child:  IconButton(
            onPressed: (){
              onTapVector();
            },
            icon:   Icon(Icons.arrow_back_ios,color:appTheme.black900,size: 18.h,)
        )),
    title: AppbarTitle(
      text: title,
    ),
  );
}
onTapVector(){
  Get.back();
}

