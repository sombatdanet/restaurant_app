import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant_app/components/custom_elevated_button.dart';
import 'package:restaurant_app/components/custom_image_view.dart';
import 'package:restaurant_app/constants/image_constant.dart';
import 'package:restaurant_app/core/utils/size_utils.dart';
import 'package:restaurant_app/routes/app_routes.dart';
import 'package:restaurant_app/themes/custom_button_style.dart';
import 'package:restaurant_app/themes/theme_helper.dart';

class CompleteBooking extends StatelessWidget {
  const CompleteBooking({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        _buildBackground(),
        Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                   Text(
                    "Congrats!",
                    style: theme.textTheme.titleMedium?.copyWith(
                      color: appTheme.deepPurple100,
                      fontSize: 30.fSize,
                    ),
                  ),
                  SizedBox(
                    height: 20.v,
                  ),
                   SizedBox(
                     width:350.h,
                     child: Text(
                      "Your order has been placed and you will get a shipping confirmation soon.",
                      textAlign: TextAlign.center,
                      style: theme.textTheme.labelMedium?.copyWith(
                        fontSize: 18.fSize,
                      ),
                     ),
                   ),
                ],
              ),
            ),
            CustomElevatedButton(
              onPressed: () {
                Get.offAllNamed(AppRoutes.homeScreenContainer);
              },
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              text: 'Shop more',
              buttonTextStyle: const TextStyle(color: Colors.white),
              buttonStyle: CustomButtonStyles.fillOrange,
            ),
          ],
        )
      ]),
    );
  }
}
Widget _buildBackground(){
  return CustomImageView(
    imagePath: ImageConstant.congrate,
  );
}
